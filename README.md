## Description.

Print a file (or some pages of it) on a printer connected to a remote computer.

It is intended for use on a local network.

---


## Requirements

Both machines must have python3 installed.

The remote computer (server) must have configured the command for printing 'lp' in gnu/linux.

The client computer must first have access to the server through an ssh connection.

The client machine must also have PyQt5 installed and the paramiko and ipcalc modules for python 3.

---

## Install

The script 'server_print.py' should be copied to the server folder '/usr/local/bin'.


The remaining scripts must go in a folder on the client machine, where the user deems it convenient..


---

## Config

Edit file script 'print_file.py' of the client.
Change the values of the following lines with those that correspond to the server.

	hostname = "192.168.0.18"
	username = "user_name"
	password = "password"
	port = 22

---

## How to use

Run the print_file.py script on the client.
For instance:
python3 print_file.py
in a terminal or any other way that the client has available to do so.

Windows will guide you.

![avatar](/img/windialog0.png)
![avatar](/img/windialog.png)

---

## Restriction

Allowed Formats:
The *lp* command *only supports text, pdf and ps files.*
It does not recognize docs, xls, or any other ... 
However, if you want to print these types of files, just convert them to pdf or ps.
For this task there are converters, such as a2ps or html2pdf.

---

## BUGS

Gives error when printing files with blank spaces in the title.
