#!/usr/bin/env
"""server_print.py

This script receives data from the client on two sockets that are opened in succession.
The first (port = 6000) receives the name of the file (without path) and the pages to be printed.
The format is a byte string where a carriage return '\\n' separates the filename from the pages to print.
Ex:  b'test.pdf \\n2,4 '  # print pages 2 and 4 from test.pdf
The second (port 7000) receives the file to print.
This is saved in the temporary folder '/ tmp'.
Ex:  /tmp/test.pdf
Then run the lp command to print the file saved in / tmp with the received parameter.
Ex: 'lp test.pdf 2,4'
After printing the script ends."""


import socket
import subprocess
import struct
# from time import time
LOCALHOST = "192.168.0.18"


def receive_file_size(sck: socket.socket):
    """Read from the socket the number of bytes that will be received from the file."""
    # Esta función se asegura de que se reciban los bytes
    # que indican el tamaño del archivo que será enviado,
    # que es codificado por el cliente vía struct.pack(),
    # función la cual genera una secuencia de bytes que
    # representan el tamaño del archivo.
    fmt = "<Q"
    expected_bytes = struct.calcsize(fmt)
    received_bytes = 0
    stream = bytes()
    while received_bytes < expected_bytes:
        chunk = sck.recv(expected_bytes - received_bytes)
        stream += chunk
        received_bytes += len(chunk)
    filesize = struct.unpack(fmt, stream)[0]
    return filesize


def receive_file(sck: socket.socket, filename):
    """Recibe un archivo en el socket sck."""
    # Leer primero del socket la cantidad de
    # bytes que se recibirán del archivo.
    filesize = receive_file_size(sck)
    # Abrir un nuevo archivo en donde guardar
    # los datos recibidos.
    with open(filename, "wb") as f:
        received_bytes = 0
        # Recibir los datos del archivo en bloques de
        # 1024 bytes hasta llegar a la cantidad de
        # bytes total informada por el cliente.
        while received_bytes < filesize:
            chunk = sck.recv(1024)
            if chunk:
                f.write(chunk)
                received_bytes += len(chunk)


if __name__ == '__main__':
    # Espera data del archivo y qué páginas imprimir.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((LOCALHOST, 6000))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                print(data)
                break

        archivo, parameter = data.decode().split('\n')

    new_location = f"/tmp/{archivo}"
    with socket.create_server((LOCALHOST, 7000)) as server:
        print(f"server: Esperando al cliente para recibir {archivo}...")
        server.settimeout(60.0)
        conn, address = server.accept()
        print(f"server: {address[0]}:{address[1]} conectado.")
        print("server: Recibiendo archivo...")
        receive_file(conn, new_location)
        print("server: Archivo recibido.")
    print("Conexión server cerrada.")

    if parameter.strip():
        print(f"Comando de impresión: 'lp {new_location} -P {parameter}'")
        subprocess.run(["lp", new_location, '-P', parameter])
    else:
        print(f"Comando de impresión: 'lp {new_location}")
        subprocess.run(["lp", new_location])
