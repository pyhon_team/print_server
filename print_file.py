""""""
import sys

import threading
import time
import subprocess
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QDesktopWidget,\
     QPushButton, QLineEdit, QFileDialog, QVBoxLayout, QHBoxLayout, QMessageBox

from pathlib import Path
import subprocess

from ipcalc import Network

import client
import ssh

from utils import log

RED = '192.168.0.0/24'

# hostname = "192.168.0.7"
# username = "elsa"
# password = "laboral"
# port = 22
hostname = "192.168.0.18"
username = "ruben"
password = "ruben794613"
port = 8118

######################################################
# En la parte grafica(App) elige un archivo y las paginas para imprimir.
# Cuando se clickea imprimir lanza el programa servidor(server.py) en la máquina server mediante ssh( la que tiene la instalada la impresora).
# Este script en el server esperará los datos necesarios para imprimir(nombre de archivo, paginas a imprimir y el archivo).
#
# Se lanza un hilo en la maquina cliente (aquí) que espera para conectarse con el servidor,
# y enviarle los datas mediante 2 socket consecutivos:
#   1º data(nombre_archivo y parametro paginas a imprimir
#   2º enviarle el archivo.
#
# El server recibe con el 1º socket el nombre del archivo(solo como referencia, no es necesario el real)
# y el parametro(las paginas a imprimir).
# Luego con el 2º socket recibe el archivo y lo guarda en la carpeta /tmp
# Luego ejecuta el comando lp para imprimir el archivo guardado en /tmp con el parametro recibido.#


class App(QWidget):
    """Application."""

    def __init__(self):
        super().__init__()
        self.title = 'Imprimir archivo de forma remota'
        self.initUI()
        log("", init=True)

    def initUI(self):
        self.setWindowTitle(self.title)
        self.resize(500, 200)
        self.center()

        hbox_search = QHBoxLayout()
        label_title_file = QLabel()
        label_title_file.setText("Archivo a imprimir")
        self.label_file = QLabel()
        self.label_file.setText("")
        hbox_search.addWidget(label_title_file)
        hbox_search.addWidget(self.label_file)

        hbox_pages = QHBoxLayout()
        label_title_pages = QLabel()
        label_title_pages.setText("Ingresar Paginas a imprimir")
        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(280, 40)
        hbox_pages.addWidget(label_title_pages)
        hbox_pages.addWidget(self.textbox)

        change_button = QPushButton("Elegir otro archivo")
        ok_button = QPushButton("Imprimir")
        cancel_button = QPushButton("Cancel")

        cancel_button.clicked.connect(self.close_win)
        ok_button.clicked.connect(self.print_file)
        change_button.clicked.connect(self.openFileNameDialog)

        hbox = QHBoxLayout()
        hbox.addWidget(change_button)
        hbox.addStretch(1)
        hbox.addWidget(ok_button)
        hbox.addStretch(1)
        hbox.addWidget(cancel_button)

        self.openFileNameDialog()
        # self.openFileNamesDialog()
        # self.saveFileDialog()

        vbox = QVBoxLayout()
        vbox.addLayout(hbox_search)
        vbox.addLayout(hbox_pages)
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        self.setLayout(vbox)
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filename, _ = QFileDialog.getOpenFileName(self,
                                                  "Seleccionar archivo a imprimir",
                                                  "",
                                                  "All Files (*);;Python Files (*.py)",
                                                  options=options)
        if filename:
            print(f"Archivo seleccionado: {filename}")
            self.label_file.setText(filename)

    def openFileNamesDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self,
                                                "QFileDialog.getOpenFileNames()",
                                                "",
                                                "All Files (*);;Python Files (*.py)",
                                                options=options)
        if files:
            print(files)

    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filename, _ = QFileDialog.getSaveFileName(self,
                                                  "QFileDialog.getSaveFileName()",
                                                  "",
                                                  "All Files (*);;Text Files (*.txt)",
                                                  options=options)
        if filename:
            print(filename)

    def print_file(self):
        """Callback de boton imprimir"""
        archivo = Path(self.label_file.text())
        paginas = self.textbox.text()
        print("Imprimir:")
        print(f"Archivo = {archivo}")
        print(f"Páginas = '{paginas}'")
        buttonReply = QMessageBox.question(self, 'Confirmar la impresión',
                                           f"Imprimir Páginas = '{paginas if paginas else 'Todas'}'\nArchivo = {archivo}",
                                           QMessageBox.Yes | QMessageBox.Cancel)

        if buttonReply == QMessageBox.Yes:
            log(f'Enviar para imprimir {archivo}, paginas {paginas}')
            launch_server(archivo, paginas)
        else:
            log('Cancel print.')

    def close_win(self) -> bool:
        """Close win"""
        self.close()


##############################################
def check_in_net(host):
    """Controla que host pertenezca a la red"""
    r = host in Network(RED)
    if not r:
        log(f"{hostname} no pertenece a la red {RED}")
        sys.exit()


def ping(host):
    n = 3
    for i in range(n):
        print(f"Intento de ping {i + 1} de {n}")
        ret = subprocess.call(['ping', '-c', '1', '-W', '5', host],
                              stdout=open('/dev/null', 'w'),
                              stderr=open('/dev/null', 'w'))
        print('!', ret)
        if not ret:
            break
    return ret == 0


def pdf_convert(f: str):
    def wrapper(*args, **kwargs):
        """Convert .docx', '.doc', '.ods' or '.odt' to pdf format"""
        archivo, data = args
        stem = archivo.stem
        extension = archivo.suffix
        if extension in ['.docx', '.doc', '.ods', '.odt']:
            print(f'Convert {archivo} to .pdf...')
            cmd = f"lowriter --convert-to pdf {archivo}"
            s = subprocess.run(["lowriter", "--convert-to", "pdf", archivo, '--outdir', '/tmp'])
            print(f"lowriter returncode={s.returncode}")
            archivo = Path(f'/tmp/{stem}.pdf')
        # los formatos de texto plano pueden imprimirse con 'lp', sin convertir.
        return f(archivo, data)
    return wrapper


@pdf_convert
def launch_server(archivo: Path, paginas: str):
    """Launch server program on the server through ssh."""
    # check server
    print(f"Chequeando a {hostname}")
    check_in_net(hostname)
    host_in_line = ping(hostname)
    if not host_in_line:
        log(f"{hostname} no está en línea.")
        sys.exit()
    log(f"{hostname} está en línea.")

    hilo1 = threading.Thread(target=send_file, args=(archivo, paginas,))
    hilo1.start()

    command = f"python3 /usr/local/bin/server_print.py"
    log(f"Ejecutando '{command}' en {username}@{hostname}\n" + 56 * '-')
    ssh_exe = ssh.SshExecute(command)
    ssh_exe.ejecutar()


def send_file(archivo: Path, data: str):
    print(f"Enviando {archivo} {data} al servidor de impresión")
    t0 = time.time()
    while True:
        try:
            client.main(archivo, data)
            break
        except ConnectionRefusedError:
            t1 = time.time()
            if t1 - t0 > 10:
                log("timeout in client")
                break
    log('Fin del hilo')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
