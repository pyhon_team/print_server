"""utils.py"""


def log(msg, init=False):
    mode = 'w' if init else 'a'
    with open('application.log', mode) as f:
        f.writelines(msg)
    print(msg)
