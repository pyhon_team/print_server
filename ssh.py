"""Lanza un programa en un equipo remoto a traves de ssh"""
import sys
import paramiko

import sys
HOSTNAME = "192.168.0.18"
USERNAME = "ruben"
PASWORD = "ruben794613"
PORT = 8118


class SshExecute():
    def __init__(self, command='ls'):
        self.host = HOSTNAME
        self.username = USERNAME
        self.password = PASWORD
        self.port = PORT

        self.client = None

        self.command = command

    def ejecutar(self):
        try:
            # Conectamos por ssh
            paramiko.util.log_to_file('paramiko.log')
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # self.client.load_system_host_keys()
            print("Conectarse al servidor mediante ssh...")
            self.client.connect(self.host, self.port, self.username, self.password)
        except paramiko.AuthenticationException:
            print("[!] Cannot connect to the SSH Server")
            print(f"Authentication failed, please verify your credentials: {paramiko.AuthenticationException}")
            sys.exit(1)
        except paramiko.SSHException as sshException:
            print("Unable to establish SSH connection: %s" % sshException)
        except paramiko.BadHostKeyException as badHostKeyException:
            print("Unable to verify server's host key: %s" % badHostKeyException)

        try:
            # Ejecutamos el comando remoto
            print("Ejecutando el comando remoto")
            stdin, stdout, stderr = self.client.exec_command(self.command)
            print(stderr.read())
            # Mostramos la salida estandar línea por línea
            for line in iter(stdout.readline, ""):
                print(line, end="")
        except paramiko.SSHException as e:
            print(f'Error: al ejecutar el comando: {e}')
            sys.exit(1)
        # ------------------------------------------------------------------
        finally:
            self.client.close()


if __name__ == '__main__':
    command = f"python3 /home/ruben/server.py"
    ssh = SshExecute(command)
    ssh.ejecutar()
