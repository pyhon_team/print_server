"""client.py"""
import os
from pathlib import Path
import socket
import struct
import time

from utils import log

LOCALHOST = "192.168.0.18"


def send_file(sck: socket.socket, filename):
    # Obtener el tamaño del archivo a enviar.
    filesize = os.path.getsize(filename)
    # Informar primero al servidor la cantidad
    # de bytes que serán enviados.
    sck.sendall(struct.pack("<Q", filesize))
    # Enviar el archivo en bloques de 1024 bytes.
    with open(filename, "rb") as f:
        while read_bytes := f.read(1024):
            sck.sendall(read_bytes)


def main(archivo="prueba.pdf", paginas="1,2"):
    nombre_archivo = Path(archivo).name
    data = f"{nombre_archivo}\n{paginas}"
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((LOCALHOST, 6000))
        log("Enviando data...", data.encode())
        s.sendall(data.encode())

    time.sleep(10) # FIXME esperar al server
    with socket.create_connection((LOCALHOST, 7000)) as conn:
        log("Conectado al servidor.")
        log("Enviando archivo...")
        send_file(conn, f"{archivo}")
        log("Enviado.")
    print("Conexión cliente cerrada.")


if __name__ == '__main__':
    main()
